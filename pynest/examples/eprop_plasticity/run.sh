#!/bin/bash

# SEEDS=( 1948523 1084434 1203245 1620104 1437782 )
# SEEDS=( 1071530 1722584 1601337 1452227 1572344 1366848 1942869 1535549 1215699 1227018 1830315 1416761 1444073 1908938 1367509 1849015 1196006 1584160 1391084 1457309 )
SEEDS=( 1071530 1722584 1601337 1452227 1572344 1366848 1942869 1535549 1215699 1830315 1416761 1444073 1908938 1367509 1849015 1196006 1584160 1391084 1457309 )

WMWR=False

if [ $WMWR = True ]; then
    dataset_dir=/home/espinoza/opt/nest/weight-consolidation/dataset
    recordings_dir=/home/espinoza/opt/nest/weight-consolidation/recordings
    weights_dir=/home/espinoza/opt/nest/weight-consolidation/weights
elif [ $WMWR = False ]; then
    dataset_dir=/home/jesus/Downloads
    recordings_dir=/home/jesus/Projects/BUW/Weight-consolidation/code/nest/weight-consolidation/recordings
    weights_dir=/home/jesus/Projects/BUW/Weight-consolidation/code/nest/weight-consolidation/weights
fi

run_eprop() {
    local weight_seed=$1
    local task_id=$2
    local n_iter=$3
    local group_size=$4
    local test=$5
    local save_weights=$6
    local permute_seed=$7

    echo ""
    echo ""
    echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo "ewc_reg_coeff_in_rec: ${ewc_reg_coeff_in_rec}"
        echo "ewc_reg_coeff_rec_out: ${ewc_reg_coeff_rec_out}"
        echo "task_id: ${task_id}"
        echo "group size: ${group_size}"
        echo "save_weights: ${save_weights}"        
        echo ">>>>>TEST: ${test}"
        echo ">>>>>permute_seed: ${permute_seed}"
    echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

     python ${script_name} \
    --weight_seed=${weight_seed} \
    --dataset_dir=${dataset_dir} \
    --recordings_dir=${recordings_dir} \
    --weights_dir=${weights_dir} \
    --eta 5e-3 \
    --sequence ${sequence} \
    --n_out ${n_out} \
    --ewc_reg_coeff_in_rec ${ewc_reg_coeff_in_rec} \
    --ewc_reg_coeff_rec_rec ${ewc_reg_coeff_rec_rec} \
    --ewc_reg_coeff_rec_out ${ewc_reg_coeff_rec_out} \
    --task_id ${task_id} \
    --n_iter ${n_iter} \
    --group_size ${group_size} \
    --permute_input_seed ${permute_seed} \
    --test ${test} \
    --save_weights ${save_weights}

    exit_code=$?

    # Check if the exit code is 13
    if [ $exit_code -eq 13 ]; then
        echo "WEIGHT SEED: ${weight_seed} DOES NOT WORK" >> ${recordings_dir}/failed_seeds.txt
    fi

}


# #---------------------------------------------------------------------------

# script_name=eprop_supervised_classification_neuromorphic_mnist_bsshslm_2020.py

# sequence=100
# n_out=10
# ewc_reg_coeff_in_rec=0
# ewc_reg_coeff_rec_rec=0
# ewc_reg_coeff_rec_out=0

# N=7
# # -------------------------------------   SEED      ID         N_ITER    GROUPSIZE   TEST    SAVE    PERM
# for seed in ${SEEDS[@]}; do
#                             run_eprop     $seed     -1         3        128           False   True    99

# for i in $(seq 0 $N); do
#                             run_eprop     $seed     $i         5        128           False   True    $i
# for j in $(seq 0 $i); do
#                             run_eprop     $seed     $((i+1))   1        512           True    False   $j
# done
# done
# done



#---------------------------------------------------------------------------

# SEEDS=( 1875688 )

script_name=eprop_supervised_regression_sine-waves.py
sequence=1000
n_out=1
ewc_reg_coeff_in_rec=200 #100
ewc_reg_coeff_rec_rec=200 #100
ewc_reg_coeff_rec_out=20 #10

N=7
# -------------------------------------   SEED      ID         N_ITER    GROUPSIZE   TEST    SAVE    PERM
for weight_seed in ${SEEDS[@]}; do
                            run_eprop     $weight_seed     -1         10        1           False   True    99

for i in $(seq 0 $N); do
                            run_eprop     $weight_seed     $i         200       1           False   True    $i
for j in $(seq 0 $i); do
                            run_eprop     $weight_seed     $((i+1))   1         1           True    False   $j
done
done
done

